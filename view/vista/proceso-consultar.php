<h1 class="page-header">
    <?php echo $proceso->id != null ? $proceso->descripcion : 'Consultar Procesos'; ?><h2></h2>
</h1>

<ol class="breadcrumb">
  <li><a href="?c=proceso" style="color:#6F282C;">Inicio</a></li>
  <li class="active"><?php echo $proceso->id != null ? $proceso->numero_proceso : 'Consultar Procesos'; ?></li>
</ol>

<table class="table  table-striped  table-hover" id="tabla" style="width:500px; " align="center" >
    <thead >
        <tr>
        <th style="width:200px; background-color: #6F282C; color:#fff">Numero Proceso</th>
            <th style=" background-color: #6F282C; color:#fff">Fecha creacion</th>
            <th style="width:60px; background-color: #6F282C; color:#fff"></th>

        </tr>
    </thead>
    <tbody>
    <?php foreach($this->model->Listar() as $r): ?>
        <tr>
         <td align="center"><?php echo "<b>".$r->id."</b>"; ?></td>

            <td align="center"><?php echo $r->fecha_creacion; ?></td>

            <td>
                <a  class="btn btn-warning pull-right" href="?c=proceso&a=Detalle&id=<?php echo $r->id; ?>">Detalle</a>
            </td>

        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<script  src="assets/js/datatable.js">

</script>
<br>
<br>
