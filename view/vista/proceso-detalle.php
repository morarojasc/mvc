<h1 class="page-header">
    Proceso:  <?php echo "fff". $proceso->id != null ? $proceso->id : 'Consultar Procesos'; ?>
</h1>

<ol class="breadcrumb">
  <li><a href="?c=proceso" style="color:#6F282C">Inicio</a></li>
  <li><a href="?c=proceso&a=Consultar" style="color:#DD7D1A">Consultar Procesos</a></li>
  <li class="active"><?php echo $proceso->id != null ? $proceso->numero_proceso : 'Consultar Procesos'; ?></li>
</ol>

<table class="table  table-striped  table-hover" id="tabla">
    <thead>
        <tr>
        <th style="width:150px; background-color: #6F282C; color:#fff">Numero Proceso</th>
            <th style="width:180px; background-color: #6F282C; color:#fff">Descripcion</th>
            <th style=" background-color: #6F282C; color:#fff">Fecha creacion</th>
            <th style=" background-color: #6F282C; color:#fff">Sede</th>
            <th style="width:120px; background-color: #6F282C; color:#fff">Presupuesto</th>
            <th style="width:60px; background-color: #6F282C; color:#fff"></th>
            <th style="width:60px; background-color: #6F282C; color:#fff"></th>
        </tr>
    </thead>
    <tbody>

        <tr>
         <td><?php echo "<b>".$proceso->id."</b>"; ?></td>
            <td><?php echo $proceso->descripcion; ?></td>
            <td><?php echo $proceso->fecha_creacion; ?></td>
            <td><?php echo $proceso->sede ; ?></td>

            <td><?php echo "<b> Usd $ ".number_format(($proceso->presupuesto/3070.65),2,",",".")."</b>";?>   </td>
            <td>
                <a  class="btn btn-warning" href="?c=proceso&a=Crud&id=<?php echo $proceso->id; ?>">Editar</a>
            </td>
            <td>
                <a  class="btn btn-danger" onclick="javascript:return confirm('¿Seguro de eliminar este proceso?');" href="?c=proceso&a=Eliminar&id=<?php echo $proceso->id; ?>">Eliminar</a>
            </td>
        </tr>
    </tbody>
</table>
